﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //var testObserver = new Behavioral.Observer.Test();
            //testObserver.Run();

            //var testCommand = new Behavioral.Command.Test();
            //testCommand.Run();

            //var singletonTest = new Creational.Singleton.Version1.Test();
            //singletonTest.Run();

            //var bridgeTest = new Structural.Bridge.Version1.Test();
            //bridgeTest.Run();

            //var abstractFactoryTest = new Creational.AbstractFactory.version1.Test();
            //abstractFactoryTest.Run();

            //var factoryMethod = new Creational.FactoryMethod.version1.Test();
            //factoryMethod.Run();

            //var objectPoolTest = new Creational.ObjectPool.version1.Test();
            //objectPoolTest.Run();

            //var pooledObjectVer2 = new  Creational.ObjectPool.version2.Test();
            //pooledObjectVer2.Run();

            //var strategyTest = new Patterns.Behavioral.Strategy.Test();
            //strategyTest.Run();

            //var decoratorTest = new Structural.Decorator.Test();
            //decoratorTest.Run();

            //var compositeTest = new Structural.Composite.Test();
            //compositeTest.Run();

            //var decoratorTest = new Structural.Decorator.PredicateDecorator.Test();
            //decoratorTest.Run();

            var decoratorTest = new Structural.Decorator.LazyDecorator.Test();
            decoratorTest.Run();

            Console.WriteLine("========================================");
            Console.WriteLine("Press ENTER");
            Console.ReadLine();
        }
    }
}
