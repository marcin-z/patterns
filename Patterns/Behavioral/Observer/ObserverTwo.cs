﻿using System;

namespace Patterns.Behavioral.Observer
{
    public class ObserverTwo : IObserver
    {
        public void Message(string message)
        {
            Console.WriteLine("Message: {0}  received by: {1}", message, "ObserverTwo");
        }
    }
}
