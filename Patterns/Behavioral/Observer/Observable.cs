﻿using System.Collections.Generic;

namespace Patterns.Behavioral.Observer
{
    public class Observable : IObservable
    {
        private readonly IList<IObserver> _observers;

        public Observable()
        {
            _observers = new List<IObserver>();
        }


        public void Add(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Remove(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify(string message)
        {
            if (_observers != null)
            {
                foreach (var observer in _observers)
                {
                    observer.Message(message);
                }
            }
        }

    }
}
