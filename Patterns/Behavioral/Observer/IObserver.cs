﻿namespace Patterns.Behavioral.Observer
{
    public interface IObserver
    {
        void Message(string message);
    }
}
