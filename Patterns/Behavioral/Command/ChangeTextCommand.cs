﻿namespace Patterns.Behavioral.Command
{
    public class ChangeTextCommand : Command
    {
        private string _previousText;
        private readonly string _newText;
        
        public ChangeTextCommand(Receiver receiver, string newText ) : base(receiver)
        {
            _newText = newText;
        }
        
        public override void Execute()
        {
            _previousText = _receiver.Text;
            _receiver.Text = _newText;
        }

        public override void UnExecute()
        {
            _receiver.Text = _previousText;
        }
    }
}
