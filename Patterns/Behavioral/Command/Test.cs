﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.Command
{
    public class Test
    {
        public void Run()
        {
            var receiver = new Receiver()
            {
                Text ="Pierwsza wersja"
            };
            Command command1 = new ChangeTextCommand(receiver,"Druga wersja");
            Command command2 = new ChangeTextCommand(receiver, "Trzecia wersja");

            var invoker = new Invoker();
            invoker.AddCommand(command1);
            invoker.AddCommand(command2);

            Console.WriteLine("Tekst początkowy:");
            Console.WriteLine(receiver.Text);

            Console.WriteLine("Tekst po zmianach:");
            invoker.ExecuteCommands();
            Console.WriteLine(receiver.Text);

            Console.WriteLine("Tekst po wycofaniu ostatniej zmiany:");
            invoker.RollbackLast();
            Console.WriteLine(receiver.Text);

            Console.WriteLine("Tekst po wycofaniu wszystkich zmian:");
            invoker.Rollback();
            Console.WriteLine(receiver.Text);

            Console.WriteLine("I ponownie tekst po zmianach:");
            invoker.ExecuteCommands();
            Console.WriteLine(receiver.Text);

            Console.WriteLine("Nacisnij ENTER");
            Console.ReadLine();
        }
    }
}
