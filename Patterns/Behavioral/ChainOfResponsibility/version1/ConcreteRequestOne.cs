﻿namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public class ConcreteRequestOne : Request
    {
        public override Category Category
        {
            get { return Category.CategoryOne; }
        }
    }
}
