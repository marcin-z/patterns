﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public abstract class Handler
    {
        private Handler _successor;
        public Handler()
        {}

        public Handler(Handler successor)
        {
            _successor = successor;
        }

        public void Handle(Request request)
        {
            OnHandle(request);
            if(!request.IsHandled)
            {
                if(_successor != null)
                {
                    if (_successor != null)
                    {
                        _successor.Handle(request);
                    }
                }
            }
        }

        protected abstract void OnHandle(Request request);
    }
}
