﻿namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public class ConcreteRequestTwo : Request
    {
        public override Category Category
        {
            get { return Category.CategoryTwo; }
        }
    }
}
