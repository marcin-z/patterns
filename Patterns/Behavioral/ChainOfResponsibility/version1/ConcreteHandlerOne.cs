﻿namespace Patterns.Behavioral.ChainOfResponsibility.version1
{
    public class ConcreteHandlerOne : Handler
    {
        public ConcreteHandlerOne()
        {}

        public ConcreteHandlerOne(Handler successor) : base(successor)
        {}

        protected override void OnHandle(Request request)
        {
            if (request.Category == Category.CategoryOne)
            {
                request.IsHandled = true;
            }
        }
    }
}
