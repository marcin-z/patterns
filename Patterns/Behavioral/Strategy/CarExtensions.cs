﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Behavioral.Strategy
{
    public static class CarExtensions
    {
        public static string GetName(this Car car)
        {
            return string.Format($"{car.Brand} {car.Model} {car.Engine}");
        }

        public static string GetFullDescription(this Car car)
        {
            return string.Format($"{car.Brand} {car.Model} {car.Engine} HP:{car.HP} Doors:{car.Doors}"); 
        }
    }
}
