﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version1
{
    public static class Pool
    {
        const int MaxPooledProducts = 5;
        static object _locker = new object();
        static bool _initialized = false;
        private static List<Product> _availableProducts = new List<Product>();
        private static List<Product> _inUseProducts = new List<Product>();


        public static Product GetPooled()
        {
            Init();
            lock (_locker)
            {
                if (_availableProducts.Count > 0)
                {
                    Product product = _availableProducts[0];
                    _inUseProducts.Add(product);
                    _availableProducts.RemoveAt(0);
                    return product;
                }
                else
                {
                    throw new Exception("Object pool exceeded");
                    //Product product = new Product();
                    //_inUseProducts.Add(product);
                    //return product;
                }
            }
        }

        public static void ReleaseInUse(Product product)
        {
            Clean(product);
            lock (_locker)
            {
                _availableProducts.Add(product);
                _inUseProducts.Remove(product);
            }
        }

        public static int CountAvailable()
        {
            return _availableProducts.Count;
        }

        public static int CountInUse()
        {
            return _inUseProducts.Count;
        }

        private static void Init()
        {
            if (!_initialized)
            {
                lock (_locker)
                {
                    if (!_initialized)
                    {
                        for (int i = 0; i < MaxPooledProducts; i++)
                        {
                            _availableProducts.Add(new Product(i) { }
                                );
                        }
                    }
                }
            }
        }

        private static void Clean(Product product)
        {
            product.SetData("Cleaned");
        }
    }
}
