﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version1
{
    public class Test
    {
        public void Run()
        {
            Console.WriteLine("Starting with {0}", Pool.CountAvailable());
            Product product1 = Pool.GetPooled();
            product1.SetData("Data for product 1");
            Product product2 = Pool.GetPooled();
            product2.SetData("Data for product 2");
            Pool.ReleaseInUse(product2);
            Product product3 = Pool.GetPooled();
            product3.SetData("Data for product 3");
        }
    }
}
