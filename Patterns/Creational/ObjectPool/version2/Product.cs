﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version2
{
    public class Product : IProduct
    {
        private static int count = 0;
        private int num;

        public Product()
        {
            num = Interlocked.Increment(ref count);
        }

        public void Test()
        {
            Console.WriteLine("Test produktu: {0}", num);
        }

        public void Dispose()
        {
            Console.WriteLine("Produkt: {0} został zwolniony", num);
        }
    }
}
