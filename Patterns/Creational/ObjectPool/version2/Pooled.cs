﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version2
{
    public class Pooled : IProduct
    {
        private Product internalFoo;
        private Pool<IProduct> pool;

        public Pooled(Pool<IProduct> pool)
        {
            if (pool == null)
                throw new ArgumentNullException("pool");

            this.pool = pool;
            this.internalFoo = new Product();
        }

        public void Dispose()
        {
            if (pool.IsDisposed)
            {
                internalFoo.Dispose();
            }
            else
            {
                pool.Release(this);
            }
        }

        public void Test()
        {
            internalFoo.Test();
        }
    }
}
