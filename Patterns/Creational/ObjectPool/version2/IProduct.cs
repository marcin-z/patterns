﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.ObjectPool.version2
{
    public interface IProduct : IDisposable
    {
        void Test();
    }
}
