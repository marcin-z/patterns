﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public abstract class AbstractFactoryKit
    {
        public abstract IProductOne CreateProductOne();
        public abstract IProductTwo CreateProductTwo();
    }
}
