﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class Test
    {
        public void Run()
        {
            Console.WriteLine("Tworzymy kliemta z pierwszą fabryką");
            Client client1 = new Client(new ConcreteFactoryKitOne());
            client1.Run();

            Console.WriteLine("Tworzymy klienta z inną fabryką");
            Client client2 = new Client(new AnotherConcreteFactoryKit());
            client2.Run();


        }
         
    }
}
