﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.AbstractFactory.version1
{
    public class ProductTwo : IProductTwo
    {
        public ProductTwo()
        {
            Console.WriteLine("ProductTwo created");
        }

        public void Consume(IProductOne productOne)
        {
            Console.WriteLine( productOne.GetType().Name +  " został użyty przez " + this.GetType().Name );
        }
    }
}
