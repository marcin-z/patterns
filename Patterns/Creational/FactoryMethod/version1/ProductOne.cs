﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.FactoryMethod.version1
{
    public class ProductOne : IProduct
    {
        public ProductOne()
        {
            Console.WriteLine("ProductOne created");
        }
    }
}
