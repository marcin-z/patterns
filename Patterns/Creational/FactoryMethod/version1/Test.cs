﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.FactoryMethod.version1
{
    public class Test
    {
        public void Run()
        {

            Console.WriteLine("Rejestracja fabryk");
            var creators = new Creator[] {
                new ConcreteCreatorOne(),
                new ConcreteCreatorTwo(),
                new ConcreteCreatorThree()
            };

            Console.WriteLine("Pobranie z trzeciej");
            var product2 = creators[2].Get();
            Console.WriteLine("Pobranie z pierwszej");
            var product0 = creators[0].Get();

        }

    }
}
