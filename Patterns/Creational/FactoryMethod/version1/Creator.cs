﻿using Patterns.Creational.FactoryMethod.version1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Creational.FactoryMethod.version1
{
    public abstract class Creator
    {
        public abstract IProduct Get();

    }
}
