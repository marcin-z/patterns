﻿using System;
using System.Collections.Generic;

namespace Patterns.Structural.Composite
{
    public class DocumentComposite : IDocument
    {
        private readonly List<IDocument> Documents;

        public DocumentComposite()
        {
            Documents = new List<IDocument>();
        }

        public void Add(IDocument document)
        {
            Documents.Add(document);
        }

        public void Remove(IDocument document)
        {
            Documents.Remove(document);
        }

        public void Process()
        {
            Documents.ForEach(x => x.Process());
            Console.WriteLine("Composite Processed");
        }
    }
}
