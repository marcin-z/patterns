﻿using System;

namespace Patterns.Structural.Composite
{
    public class Document : IDocument
    {
        private string _idx = "";

        public Document(string idx)
        {
            _idx = idx;
        }
        public void Process()
        {
            Console.WriteLine("Document {0} processed",_idx);
        }
    }
}
