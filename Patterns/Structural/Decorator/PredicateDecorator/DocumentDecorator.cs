﻿using System;

namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public class DocumentDecorator : IDocument
    {
        private readonly IDocument _document;
        private readonly IPredicate _predicate;
        public DocumentDecorator(IDocument document, IPredicate predicate)
        {
            _document = document;
            _predicate = predicate;
        }

        public string GetSignature()
        {
            return _document.GetSignature();
        }

        public void Process()
        {
            if (_predicate.Test(this))
            {
                _document.Process();
            }
        }
    }
}
