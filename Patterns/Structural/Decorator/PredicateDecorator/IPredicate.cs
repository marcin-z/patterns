﻿namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public interface IPredicate
    {
        bool Test(IDocument document);

    }
}
