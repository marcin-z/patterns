﻿using System;

namespace Patterns.Structural.Decorator.PredicateDecorator
{
    public class Document : IDocument
    {
        private readonly string _signature;

        public Document(string signature)
        {
            _signature = signature;
        }

        public string GetSignature()
        {
            return _signature;
        }

        public void Process()
        {
            Console.WriteLine("Base document processed ({0})", GetSignature());
        }
    }
}
