﻿namespace Patterns.Structural.Decorator.LazyDecorator
{
    public interface IDocument
    {
        void Process();
    }
}
