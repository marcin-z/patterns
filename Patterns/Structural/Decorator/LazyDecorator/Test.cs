﻿using System;

namespace Patterns.Structural.Decorator.LazyDecorator
{
    public class Test
    {
        public void Run()
        {
            var decorDoc = new DocumentDecorator(new Lazy<Document>());
            decorDoc.Process();
        }

    }
}
