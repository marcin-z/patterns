﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Bridge.Version1
{
    public class Test
    {
        public void Run()
        {
            CustomersRefined customers = new CustomersRefined("Ekipa");

            customers.Data = new CustomersData();
            
            customers.Show();
            customers.Next();
            customers.Show();
            customers.Next();
            customers.Show();
            customers.Add(new Customer() { Id = 100, Name = "Stefan" });
            customers.ShowAll();

        }

    }
}
